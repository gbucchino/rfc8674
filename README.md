# Introduction
This project implement the Preference HTTP Header defined in the [RFC 8674](https://tools.ietf.org/html/rfc8674). For tha,t I maked an HTTP Website in Python3 with Flask.

# Testing
For testing, you must install the package Flask: `pip3 install flask` and, you can start with this:

```
$ python3 -m rfc6486
 * Serving Flask app "website.main" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
```

And for testing, we using a HTTP client with `curl`:

```
curl --header "Prefer: safe" http:/localhost:5000
```

