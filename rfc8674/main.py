# coding: utf-8

from flask import Flask, render_template, redirect, url_for, request
from rfc8674.config import config
app = Flask(__name__)

@app.route("/")
def root():
	return redirect(url_for('home'))

@app.route("/home")
def home():
	data = {}
	data["title"] = config["title"]
	data["safe"] = getHeaderPrefer(request.headers)
	
	return render_template('home.html', data=data)

@app.route("/blog")
def blog():
	data = {}
	data["title"] = config["title"]
	data["safe"] = getHeaderPrefer(request.headers)

	data["articles"] = []
	for entry in range(0,5):
		article = {
			"title": f"Article { str(entry) }",
			"link": f"/blog/{ str(entry) }"
		}
		data["articles"].append(article)
	return render_template('blog.html', data=data)

def getHeaderPrefer(header):
	if header.get("Prefer") is not None:
		if header.get("Prefer").lower() == "safe":
			return True
	return False

def main():
	app.run()
